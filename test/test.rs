// Copyright 2021 Ian Jackson and contributors
// SPDX-License-Identifier: GPL-3.0-or-later
// There is NO WARRANTY.

pub use easy_ext::ext;

mod tgenerics;
mod treborrow;

#[test]
#[cfg(not(miri))]
fn reject() { trybuild::TestCases::new().compile_fail("reject/*.rs"); }
