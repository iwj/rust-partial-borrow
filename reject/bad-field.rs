// Copyright 2021 Ian Jackson and contributors
// SPDX-License-Identifier: GPL-3.0-or-later
// There is NO WARRANTY.

use partial_borrow::prelude::*;

#[derive(PartialBorrow)]
struct X { }

type N = partial!(X mut f);

fn main(){
  
}
