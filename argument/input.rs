// Copyright 2021 Ian Jackson and contributors
// SPDX-License-Identifier: GPL-3.0-or-later
// There is NO WARRANTY.

use partial_borrow::prelude::*;
use partial_borrow::imports::*;

#[derive(PartialBorrow)]
#[partial_borrow(Debug)]
#[partial_borrow(imported_for_test_unsafe)]
#[partial_borrow(partial="Partial",module="Module",suffix="")]
pub struct Struct {
  a: String,
}

fn main(){ }
