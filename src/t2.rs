
use crate as partial_borrow;

//#[derive(Debug)]
#[derive(Debug)]
pub struct ABC<'d, C, const E: u8>
  where C: Clone
{
  a: usize,
  a0: X,
  pub b: String,
  pub c: C,
  pub d: &'d str,
  x: X,
}

#[derive(Debug)]
struct X;
impl X { fn use_mut(&mut self) -> &'static str { "X" } }

#[allow(dead_code)]
#[allow(non_camel_case_types)]
#[repr(C)]
pub struct ABC__Partial<'d, _P_a, _P_a0, _P_b, _P_c, _P_d, _P_x, C, const E: u8>
{
  a: ABC__::F_a<'d, _P_a, usize, C, E>,
  a0: ABC__::F_a0<'d, _P_a0, X, C, E>,
  pub b: ABC__::F_b<'d, _P_b, String, C, E>,
  pub c: ABC__::F_c<'d, _P_c, C, C, E>,
  pub d: ABC__::F_d<'d, _P_d, &'d str, C, E>,
  x: ABC__::F_x<'d, _P_x, X, C, E>,
}

impl<'d, C, const E: u8> partial_borrow::PartialBorrow for ABC<'d, C, E>
where
  C: Clone,
{
  type All_Not = ABC__Partial<
    'd,
    partial_borrow::perms::Not,
    partial_borrow::perms::Not,
    partial_borrow::perms::Not,
    partial_borrow::perms::Not,
    partial_borrow::perms::Not,
    partial_borrow::perms::Not,
    C,
    E,
  >;
  type All_Ref = ABC__Partial<
    'd,
    partial_borrow::perms::Ref,
    partial_borrow::perms::Ref,
    partial_borrow::perms::Ref,
    partial_borrow::perms::Ref,
    partial_borrow::perms::Ref,
    partial_borrow::perms::Ref,
    C,
    E,
  >;
  type All_Mut = ABC__Partial<
    'd,
    partial_borrow::perms::Mut,
    partial_borrow::perms::Mut,
    partial_borrow::perms::Mut,
    partial_borrow::perms::Mut,
    partial_borrow::perms::Mut,
    partial_borrow::perms::Mut,
    C,
    E,
  >;
  type Fields = ABC__::Fields;
  const FIELDS: Self::Fields = ABC__::FIELDS;
}

#[allow(dead_code)]
#[allow(non_camel_case_types)]
#[allow(non_snake_case)]
pub mod ABC__ {
  use super::partial_borrow::memoffset::offset_of;
  use super::partial_borrow::perms::*;
  use super::partial_borrow::{Downgrade, SplitInto, SplitOff};
  use std::marker::PhantomData;
  use std::mem;
  use std::ops::{Deref, DerefMut};

  #[repr(C)]
  pub struct F_a<'d, _P, _T, C, const E: u8>
  where
    C: Clone,
  {
    p: _P,
    t: PhantomData<_T>,
    s: PhantomData<*const super::ABC<'d, C, E>>,
  }

  impl<'d, _P, _T, C, const E: u8> Deref for F_a<'d, _P, _T, C, E>
  where
    _P: IsRef,
    C: Clone,
  {
    type Target = _T;

    fn deref(&self) -> &_T
    where
      _T: Sized,
    {
      let p: *const Self = self;
      let offset = offset_of!(super::ABC<'d, C, E>, a);
      let p: *const u8 = p as _;
      let p = unsafe { p.add(offset) };
      let p: *const _T = p as _;
      let p: &_T = unsafe { p.as_ref().unwrap() };
      p
    }
  }

  impl<'d, _P, _T, C, const E: u8> DerefMut for F_a<'d, _P, _T, C, E>
  where
    _P: IsMut,
    C: Clone,
  {
    fn deref_mut(&mut self) -> &mut _T
    where
      _T: Sized,
    {
      let p: *mut Self = self;
      let offset = offset_of!(super::ABC<'d, C, E>, a);
      let p: *mut u8 = p as _;
      let p = unsafe { p.add(offset) };
      let p: *mut _T = p as _;
      let p: &mut _T = unsafe { p.as_mut().unwrap() };
      p
    }
  }

  #[repr(C)]
  pub struct F_a0<'d, _P, _T, C, const E: u8>
  where
    C: Clone,
  {
    p: _P,
    t: PhantomData<_T>,
    s: PhantomData<*const super::ABC<'d, C, E>>,
  }

  impl<'d, _P, _T, C, const E: u8> Deref for F_a0<'d, _P, _T, C, E>
  where
    _P: IsRef,
    C: Clone,
  {
    type Target = _T;

    fn deref(&self) -> &_T
    where
      _T: Sized,
    {
      let p: *const Self = self;
      let offset = offset_of!(super::ABC<'d, C, E>, a0);
      let p: *const u8 = p as _;
      let p = unsafe { p.add(offset) };
      let p: *const _T = p as _;
      let p: &_T = unsafe { p.as_ref().unwrap() };
      p
    }
  }

  impl<'d, _P, _T, C, const E: u8> DerefMut for F_a0<'d, _P, _T, C, E>
  where
    _P: IsMut,
    C: Clone,
  {
    fn deref_mut(&mut self) -> &mut _T
    where
      _T: Sized,
    {
      let p: *mut Self = self;
      let offset = offset_of!(super::ABC<'d, C, E>, a0);
      let p: *mut u8 = p as _;
      let p = unsafe { p.add(offset) };
      let p: *mut _T = p as _;
      let p: &mut _T = unsafe { p.as_mut().unwrap() };
      p
    }
  }

  #[repr(C)]
  pub struct F_b<'d, _P, _T, C, const E: u8>
  where
    C: Clone,
  {
    p: _P,
    t: PhantomData<_T>,
    s: PhantomData<*const super::ABC<'d, C, E>>,
  }

  impl<'d, _P, _T, C, const E: u8> Deref for F_b<'d, _P, _T, C, E>
  where
    _P: IsRef,
    C: Clone,
  {
    type Target = _T;

    fn deref(&self) -> &_T
    where
      _T: Sized,
    {
      let p: *const Self = self;
      let offset = offset_of!(super::ABC<'d, C, E>, b);
      let p: *const u8 = p as _;
      let p = unsafe { p.add(offset) };
      let p: *const _T = p as _;
      let p: &_T = unsafe { p.as_ref().unwrap() };
      p
    }
  }

  impl<'d, _P, _T, C, const E: u8> DerefMut for F_b<'d, _P, _T, C, E>
  where
    _P: IsMut,
    C: Clone,
  {
    fn deref_mut(&mut self) -> &mut _T
    where
      _T: Sized,
    {
      let p: *mut Self = self;
      let offset = offset_of!(super::ABC<'d, C, E>, b);
      let p: *mut u8 = p as _;
      let p = unsafe { p.add(offset) };
      let p: *mut _T = p as _;
      let p: &mut _T = unsafe { p.as_mut().unwrap() };
      p
    }
  }

  #[repr(C)]
  pub struct F_c<'d, _P, _T, C, const E: u8>
  where
    C: Clone,
  {
    p: _P,
    t: PhantomData<_T>,
    s: PhantomData<*const super::ABC<'d, C, E>>,
  }

  impl<'d, _P, _T, C, const E: u8> Deref for F_c<'d, _P, _T, C, E>
  where
    _P: IsRef,
    C: Clone,
  {
    type Target = _T;

    fn deref(&self) -> &_T
    where
      _T: Sized,
    {
      let p: *const Self = self;
      let offset = offset_of!(super::ABC<'d, C, E>, c);
      let p: *const u8 = p as _;
      let p = unsafe { p.add(offset) };
      let p: *const _T = p as _;
      let p: &_T = unsafe { p.as_ref().unwrap() };
      p
    }
  }

  impl<'d, _P, _T, C, const E: u8> DerefMut for F_c<'d, _P, _T, C, E>
  where
    _P: IsMut,
    C: Clone,
  {
    fn deref_mut(&mut self) -> &mut _T
    where
      _T: Sized,
    {
      let p: *mut Self = self;
      let offset = offset_of!(super::ABC<'d, C, E>, c);
      let p: *mut u8 = p as _;
      let p = unsafe { p.add(offset) };
      let p: *mut _T = p as _;
      let p: &mut _T = unsafe { p.as_mut().unwrap() };
      p
    }
  }

  #[repr(C)]
  pub struct F_d<'d, _P, _T, C, const E: u8>
  where
    C: Clone,
  {
    p: _P,
    t: PhantomData<_T>,
    s: PhantomData<*const super::ABC<'d, C, E>>,
  }

  impl<'d, _P, _T, C, const E: u8> Deref for F_d<'d, _P, _T, C, E>
  where
    _P: IsRef,
    C: Clone,
  {
    type Target = _T;

    fn deref(&self) -> &_T
    where
      _T: Sized,
    {
      let p: *const Self = self;
      let offset = offset_of!(super::ABC<'d, C, E>, d);
      let p: *const u8 = p as _;
      let p = unsafe { p.add(offset) };
      let p: *const _T = p as _;
      let p: &_T = unsafe { p.as_ref().unwrap() };
      p
    }
  }

  impl<'d, _P, _T, C, const E: u8> DerefMut for F_d<'d, _P, _T, C, E>
  where
    _P: IsMut,
    C: Clone,
  {
    fn deref_mut(&mut self) -> &mut _T
    where
      _T: Sized,
    {
      let p: *mut Self = self;
      let offset = offset_of!(super::ABC<'d, C, E>, d);
      let p: *mut u8 = p as _;
      let p = unsafe { p.add(offset) };
      let p: *mut _T = p as _;
      let p: &mut _T = unsafe { p.as_mut().unwrap() };
      p
    }
  }

  #[repr(C)]
  pub struct F_x<'d, _P, _T, C, const E: u8>
  where
    C: Clone,
  {
    p: _P,
    t: PhantomData<_T>,
    s: PhantomData<*const super::ABC<'d, C, E>>,
  }

  impl<'d, _P, _T, C, const E: u8> Deref for F_x<'d, _P, _T, C, E>
  where
    _P: IsRef,
    C: Clone,
  {
    type Target = _T;

    fn deref(&self) -> &_T
    where
      _T: Sized,
    {
      let p: *const Self = self;
      let offset = offset_of!(super::ABC<'d, C, E>, x);
      let p: *const u8 = p as _;
      let p = unsafe { p.add(offset) };
      let p: *const _T = p as _;
      let p: &_T = unsafe { p.as_ref().unwrap() };
      p
    }
  }

  impl<'d, _P, _T, C, const E: u8> DerefMut for F_x<'d, _P, _T, C, E>
  where
    _P: IsMut,
    C: Clone,
  {
    fn deref_mut(&mut self) -> &mut _T
    where
      _T: Sized,
    {
      let p: *mut Self = self;
      let offset = offset_of!(super::ABC<'d, C, E>, x);
      let p: *mut u8 = p as _;
      let p = unsafe { p.add(offset) };
      let p: *mut _T = p as _;
      let p: &mut _T = unsafe { p.as_mut().unwrap() };
      p
    }
  }

  impl<
      'd,
      _R_a,
      _R_a0,
      _R_b,
      _R_c,
      _R_d,
      _R_x,
      _P_a,
      _P_a0,
      _P_b,
      _P_c,
      _P_d,
      _P_x,
      C,
      const E: u8,
    >
    AsRef<super::ABC__Partial<'d, _R_a, _R_a0, _R_b, _R_c, _R_d, _R_x, C, E>>
    for super::ABC__Partial<'d, _P_a, _P_a0, _P_b, _P_c, _P_d, _P_x, C, E>
  where
    _R_a: IsDowngradeFrom<_P_a>,
    _R_a0: IsDowngradeFrom<_P_a0>,
    _R_b: IsDowngradeFrom<_P_b>,
    _R_c: IsDowngradeFrom<_P_c>,
    _R_d: IsDowngradeFrom<_P_d>,
    _R_x: IsDowngradeFrom<_P_x>,
    C: Clone,
  {
    fn as_ref(
      &self,
    ) -> &super::ABC__Partial<'d, _R_a, _R_a0, _R_b, _R_c, _R_d, _R_x, C, E>
    {
      Downgrade::downgrade(self)
    }
  }

  impl<
      'd,
      _R_a,
      _R_a0,
      _R_b,
      _R_c,
      _R_d,
      _R_x,
      _P_a,
      _P_a0,
      _P_b,
      _P_c,
      _P_d,
      _P_x,
      C,
      const E: u8,
    >
    AsMut<super::ABC__Partial<'d, _R_a, _R_a0, _R_b, _R_c, _R_d, _R_x, C, E>>
    for super::ABC__Partial<'d, _P_a, _P_a0, _P_b, _P_c, _P_d, _P_x, C, E>
  where
    _R_a: IsDowngradeFrom<_P_a>,
    _R_a0: IsDowngradeFrom<_P_a0>,
    _R_b: IsDowngradeFrom<_P_b>,
    _R_c: IsDowngradeFrom<_P_c>,
    _R_d: IsDowngradeFrom<_P_d>,
    _R_x: IsDowngradeFrom<_P_x>,
    C: Clone,
  {
    fn as_mut(
      &mut self,
    ) -> &mut super::ABC__Partial<'d, _R_a, _R_a0, _R_b, _R_c, _R_d, _R_x, C, E>
    {
      Downgrade::downgrade_mut(self)
    }
  }

  impl<
      'd,
      _R_a,
      _R_a0,
      _R_b,
      _R_c,
      _R_d,
      _R_x,
      _P_a,
      _P_a0,
      _P_b,
      _P_c,
      _P_d,
      _P_x,
      C,
      const E: u8,
    >
    Downgrade<
      super::ABC__Partial<'d, _R_a, _R_a0, _R_b, _R_c, _R_d, _R_x, C, E>,
    > for super::ABC__Partial<'d, _P_a, _P_a0, _P_b, _P_c, _P_d, _P_x, C, E>
  where
    _R_a: IsDowngradeFrom<_P_a>,
    _R_a0: IsDowngradeFrom<_P_a0>,
    _R_b: IsDowngradeFrom<_P_b>,
    _R_c: IsDowngradeFrom<_P_c>,
    _R_d: IsDowngradeFrom<_P_d>,
    _R_x: IsDowngradeFrom<_P_x>,
    C: Clone,
  {
    fn downgrade(
      input: &Self,
    ) -> &super::ABC__Partial<'d, _R_a, _R_a0, _R_b, _R_c, _R_d, _R_x, C, E>
    {
      unsafe { mem::transmute(input) }
    }

    fn downgrade_mut(
      input: &mut Self,
    ) -> &mut super::ABC__Partial<'d, _R_a, _R_a0, _R_b, _R_c, _R_d, _R_x, C, E>
    {
      unsafe { mem::transmute(input) }
    }
  }

  impl<
      'd,
      _R_a,
      _R_a0,
      _R_b,
      _R_c,
      _R_d,
      _R_x,
      _P_a,
      _P_a0,
      _P_b,
      _P_c,
      _P_d,
      _P_x,
      C,
      const E: u8,
    >
    SplitOff<super::ABC__Partial<'d, _R_a, _R_a0, _R_b, _R_c, _R_d, _R_x, C, E>>
    for super::ABC__Partial<'d, _P_a, _P_a0, _P_b, _P_c, _P_d, _P_x, C, E>
  where
    _R_a: IsDowngradeFrom<_P_a>,
    _R_a0: IsDowngradeFrom<_P_a0>,
    _R_b: IsDowngradeFrom<_P_b>,
    _R_c: IsDowngradeFrom<_P_c>,
    _R_d: IsDowngradeFrom<_P_d>,
    _R_x: IsDowngradeFrom<_P_x>,
    C: Clone,
  {
    type Remaining = super::ABC__Partial<
      'd,
      <_R_a as IsDowngradeFrom<_P_a>>::Remaining,
      <_R_a0 as IsDowngradeFrom<_P_a0>>::Remaining,
      <_R_b as IsDowngradeFrom<_P_b>>::Remaining,
      <_R_c as IsDowngradeFrom<_P_c>>::Remaining,
      <_R_d as IsDowngradeFrom<_P_d>>::Remaining,
      <_R_x as IsDowngradeFrom<_P_x>>::Remaining,
      C,
      E,
    >;

    fn split_off<'__r>(
      input: &'__r super::ABC__Partial<
        'd,
        _P_a,
        _P_a0,
        _P_b,
        _P_c,
        _P_d,
        _P_x,
        C,
        E,
      >,
    ) -> (
      &'__r super::ABC__Partial<'d, _R_a, _R_a0, _R_b, _R_c, _R_d, _R_x, C, E>,
      &'__r Self::Remaining,
    ) {
      unsafe {
        let input = input as *const _;
        (mem::transmute(input), mem::transmute(input))
      }
    }

    fn split_off_mut<'__r>(
      input: &'__r mut super::ABC__Partial<
        'd,
        _P_a,
        _P_a0,
        _P_b,
        _P_c,
        _P_d,
        _P_x,
        C,
        E,
      >,
    ) -> (
      &'__r mut super::ABC__Partial<
        'd,
        _R_a,
        _R_a0,
        _R_b,
        _R_c,
        _R_d,
        _R_x,
        C,
        E,
      >,
      &'__r mut Self::Remaining,
    ) {
      unsafe {
        let input = input as *mut _;
        (mem::transmute(input), mem::transmute(input))
      }
    }
  }

  impl<
      'd,
      '__r,
      _R_a,
      _R_a0,
      _R_b,
      _R_c,
      _R_d,
      _R_x,
      _S_a,
      _S_a0,
      _S_b,
      _S_c,
      _S_d,
      _S_x,
      _P_a,
      _P_a0,
      _P_b,
      _P_c,
      _P_d,
      _P_x,
      C,
      const E: u8,
    >
    From<
      &'__r super::ABC__Partial<'d, _P_a, _P_a0, _P_b, _P_c, _P_d, _P_x, C, E>,
    >
    for (
      &'__r super::ABC__Partial<'d, _R_a, _R_a0, _R_b, _R_c, _R_d, _R_x, C, E>,
      &'__r super::ABC__Partial<'d, _S_a, _S_a0, _S_b, _S_c, _S_d, _S_x, C, E>,
    )
  where
    _P_a: CanSplitInto<_R_a, _S_a>,
    _P_a0: CanSplitInto<_R_a0, _S_a0>,
    _P_b: CanSplitInto<_R_b, _S_b>,
    _P_c: CanSplitInto<_R_c, _S_c>,
    _P_d: CanSplitInto<_R_d, _S_d>,
    _P_x: CanSplitInto<_R_x, _S_x>,
    C: Clone,
  {
    fn from(
      input: &'__r super::ABC__Partial<
        'd,
        _P_a,
        _P_a0,
        _P_b,
        _P_c,
        _P_d,
        _P_x,
        C,
        E,
      >,
    ) -> Self {
      SplitInto::split_into(input)
    }
  }

  impl<
      'd,
      '__r,
      _R_a,
      _R_a0,
      _R_b,
      _R_c,
      _R_d,
      _R_x,
      _S_a,
      _S_a0,
      _S_b,
      _S_c,
      _S_d,
      _S_x,
      _P_a,
      _P_a0,
      _P_b,
      _P_c,
      _P_d,
      _P_x,
      C,
      const E: u8,
    >
    From<
      &'__r mut super::ABC__Partial<
        'd,
        _P_a,
        _P_a0,
        _P_b,
        _P_c,
        _P_d,
        _P_x,
        C,
        E,
      >,
    >
    for (
      &'__r mut super::ABC__Partial<
        'd,
        _R_a,
        _R_a0,
        _R_b,
        _R_c,
        _R_d,
        _R_x,
        C,
        E,
      >,
      &'__r mut super::ABC__Partial<
        'd,
        _S_a,
        _S_a0,
        _S_b,
        _S_c,
        _S_d,
        _S_x,
        C,
        E,
      >,
    )
  where
    _P_a: CanSplitInto<_R_a, _S_a>,
    _P_a0: CanSplitInto<_R_a0, _S_a0>,
    _P_b: CanSplitInto<_R_b, _S_b>,
    _P_c: CanSplitInto<_R_c, _S_c>,
    _P_d: CanSplitInto<_R_d, _S_d>,
    _P_x: CanSplitInto<_R_x, _S_x>,
    C: Clone,
  {
    fn from(
      input: &'__r mut super::ABC__Partial<
        'd,
        _P_a,
        _P_a0,
        _P_b,
        _P_c,
        _P_d,
        _P_x,
        C,
        E,
      >,
    ) -> Self {
      SplitInto::split_into_mut(input)
    }
  }

  impl<
      'd,
      _R_a,
      _R_a0,
      _R_b,
      _R_c,
      _R_d,
      _R_x,
      _S_a,
      _S_a0,
      _S_b,
      _S_c,
      _S_d,
      _S_x,
      _P_a,
      _P_a0,
      _P_b,
      _P_c,
      _P_d,
      _P_x,
      C,
      const E: u8,
    >
    SplitInto<
      super::ABC__Partial<'d, _R_a, _R_a0, _R_b, _R_c, _R_d, _R_x, C, E>,
      super::ABC__Partial<'d, _S_a, _S_a0, _S_b, _S_c, _S_d, _S_x, C, E>,
    > for super::ABC__Partial<'d, _P_a, _P_a0, _P_b, _P_c, _P_d, _P_x, C, E>
  where
    _P_a: CanSplitInto<_R_a, _S_a>,
    _P_a0: CanSplitInto<_R_a0, _S_a0>,
    _P_b: CanSplitInto<_R_b, _S_b>,
    _P_c: CanSplitInto<_R_c, _S_c>,
    _P_d: CanSplitInto<_R_d, _S_d>,
    _P_x: CanSplitInto<_R_x, _S_x>,
    C: Clone,
  {
    fn split_into<'__r>(
      input: &'__r super::ABC__Partial<
        'd,
        _P_a,
        _P_a0,
        _P_b,
        _P_c,
        _P_d,
        _P_x,
        C,
        E,
      >,
    ) -> (
      &'__r super::ABC__Partial<'d, _R_a, _R_a0, _R_b, _R_c, _R_d, _R_x, C, E>,
      &'__r super::ABC__Partial<'d, _S_a, _S_a0, _S_b, _S_c, _S_d, _S_x, C, E>,
    ) {
      unsafe {
        let input = input as *const _;
        (mem::transmute(input), mem::transmute(input))
      }
    }

    fn split_into_mut<'__r>(
      input: &'__r mut super::ABC__Partial<
        'd,
        _P_a,
        _P_a0,
        _P_b,
        _P_c,
        _P_d,
        _P_x,
        C,
        E,
      >,
    ) -> (
      &'__r mut super::ABC__Partial<
        'd,
        _R_a,
        _R_a0,
        _R_b,
        _R_c,
        _R_d,
        _R_x,
        C,
        E,
      >,
      &'__r mut super::ABC__Partial<
        'd,
        _S_a,
        _S_a0,
        _S_b,
        _S_c,
        _S_d,
        _S_x,
        C,
        E,
      >,
    ) {
      unsafe {
        let input = input as *mut _;
        (mem::transmute(input), mem::transmute(input))
      }
    }
  }

  impl<'d, _R_a, _R_a0, _R_b, _R_c, _R_d, _R_x, C, const E: u8>
    AsRef<super::ABC__Partial<'d, _R_a, _R_a0, _R_b, _R_c, _R_d, _R_x, C, E>>
    for super::ABC<'d, C, E>
  where
    _R_a: IsDowngradeFrom<Mut>,
    _R_a0: IsDowngradeFrom<Mut>,
    _R_b: IsDowngradeFrom<Mut>,
    _R_c: IsDowngradeFrom<Mut>,
    _R_d: IsDowngradeFrom<Mut>,
    _R_x: IsDowngradeFrom<Mut>,
    C: Clone,
  {
    fn as_ref(
      &self,
    ) -> &super::ABC__Partial<'d, _R_a, _R_a0, _R_b, _R_c, _R_d, _R_x, C, E>
    {
      Downgrade::downgrade(self)
    }
  }

  impl<'d, _R_a, _R_a0, _R_b, _R_c, _R_d, _R_x, C, const E: u8>
    AsMut<super::ABC__Partial<'d, _R_a, _R_a0, _R_b, _R_c, _R_d, _R_x, C, E>>
    for super::ABC<'d, C, E>
  where
    _R_a: IsDowngradeFrom<Mut>,
    _R_a0: IsDowngradeFrom<Mut>,
    _R_b: IsDowngradeFrom<Mut>,
    _R_c: IsDowngradeFrom<Mut>,
    _R_d: IsDowngradeFrom<Mut>,
    _R_x: IsDowngradeFrom<Mut>,
    C: Clone,
  {
    fn as_mut(
      &mut self,
    ) -> &mut super::ABC__Partial<'d, _R_a, _R_a0, _R_b, _R_c, _R_d, _R_x, C, E>
    {
      Downgrade::downgrade_mut(self)
    }
  }

  impl<'d, _R_a, _R_a0, _R_b, _R_c, _R_d, _R_x, C, const E: u8>
    Downgrade<
      super::ABC__Partial<'d, _R_a, _R_a0, _R_b, _R_c, _R_d, _R_x, C, E>,
    > for super::ABC<'d, C, E>
  where
    _R_a: IsDowngradeFrom<Mut>,
    _R_a0: IsDowngradeFrom<Mut>,
    _R_b: IsDowngradeFrom<Mut>,
    _R_c: IsDowngradeFrom<Mut>,
    _R_d: IsDowngradeFrom<Mut>,
    _R_x: IsDowngradeFrom<Mut>,
    C: Clone,
  {
    fn downgrade(
      input: &Self,
    ) -> &super::ABC__Partial<'d, _R_a, _R_a0, _R_b, _R_c, _R_d, _R_x, C, E>
    {
      unsafe { mem::transmute(input) }
    }

    fn downgrade_mut(
      input: &mut Self,
    ) -> &mut super::ABC__Partial<'d, _R_a, _R_a0, _R_b, _R_c, _R_d, _R_x, C, E>
    {
      unsafe { mem::transmute(input) }
    }
  }

  impl<'d, _R_a, _R_a0, _R_b, _R_c, _R_d, _R_x, C, const E: u8>
    SplitOff<super::ABC__Partial<'d, _R_a, _R_a0, _R_b, _R_c, _R_d, _R_x, C, E>>
    for super::ABC<'d, C, E>
  where
    _R_a: IsDowngradeFrom<Mut>,
    _R_a0: IsDowngradeFrom<Mut>,
    _R_b: IsDowngradeFrom<Mut>,
    _R_c: IsDowngradeFrom<Mut>,
    _R_d: IsDowngradeFrom<Mut>,
    _R_x: IsDowngradeFrom<Mut>,
    C: Clone,
  {
    type Remaining = super::ABC__Partial<
      'd,
      <_R_a as IsDowngradeFrom<Mut>>::Remaining,
      <_R_a0 as IsDowngradeFrom<Mut>>::Remaining,
      <_R_b as IsDowngradeFrom<Mut>>::Remaining,
      <_R_c as IsDowngradeFrom<Mut>>::Remaining,
      <_R_d as IsDowngradeFrom<Mut>>::Remaining,
      <_R_x as IsDowngradeFrom<Mut>>::Remaining,
      C,
      E,
    >;

    fn split_off<'__r>(
      input: &'__r super::ABC<'d, C, E>,
    ) -> (
      &'__r super::ABC__Partial<'d, _R_a, _R_a0, _R_b, _R_c, _R_d, _R_x, C, E>,
      &'__r Self::Remaining,
    ) {
      unsafe {
        let input = input as *const _;
        (mem::transmute(input), mem::transmute(input))
      }
    }

    fn split_off_mut<'__r>(
      input: &'__r mut super::ABC<'d, C, E>,
    ) -> (
      &'__r mut super::ABC__Partial<
        'd,
        _R_a,
        _R_a0,
        _R_b,
        _R_c,
        _R_d,
        _R_x,
        C,
        E,
      >,
      &'__r mut Self::Remaining,
    ) {
      unsafe {
        let input = input as *mut _;
        (mem::transmute(input), mem::transmute(input))
      }
    }
  }

  impl<
      'd,
      '__r,
      _R_a,
      _R_a0,
      _R_b,
      _R_c,
      _R_d,
      _R_x,
      _S_a,
      _S_a0,
      _S_b,
      _S_c,
      _S_d,
      _S_x,
      C,
      const E: u8,
    > From<&'__r super::ABC<'d, C, E>>
    for (
      &'__r super::ABC__Partial<'d, _R_a, _R_a0, _R_b, _R_c, _R_d, _R_x, C, E>,
      &'__r super::ABC__Partial<'d, _S_a, _S_a0, _S_b, _S_c, _S_d, _S_x, C, E>,
    )
  where
    Mut: CanSplitInto<_R_a, _S_a>,
    Mut: CanSplitInto<_R_a0, _S_a0>,
    Mut: CanSplitInto<_R_b, _S_b>,
    Mut: CanSplitInto<_R_c, _S_c>,
    Mut: CanSplitInto<_R_d, _S_d>,
    Mut: CanSplitInto<_R_x, _S_x>,
    C: Clone,
  {
    fn from(input: &'__r super::ABC<'d, C, E>) -> Self {
      SplitInto::split_into(input)
    }
  }

  impl<
      'd,
      '__r,
      _R_a,
      _R_a0,
      _R_b,
      _R_c,
      _R_d,
      _R_x,
      _S_a,
      _S_a0,
      _S_b,
      _S_c,
      _S_d,
      _S_x,
      C,
      const E: u8,
    > From<&'__r mut super::ABC<'d, C, E>>
    for (
      &'__r mut super::ABC__Partial<
        'd,
        _R_a,
        _R_a0,
        _R_b,
        _R_c,
        _R_d,
        _R_x,
        C,
        E,
      >,
      &'__r mut super::ABC__Partial<
        'd,
        _S_a,
        _S_a0,
        _S_b,
        _S_c,
        _S_d,
        _S_x,
        C,
        E,
      >,
    )
  where
    Mut: CanSplitInto<_R_a, _S_a>,
    Mut: CanSplitInto<_R_a0, _S_a0>,
    Mut: CanSplitInto<_R_b, _S_b>,
    Mut: CanSplitInto<_R_c, _S_c>,
    Mut: CanSplitInto<_R_d, _S_d>,
    Mut: CanSplitInto<_R_x, _S_x>,
    C: Clone,
  {
    fn from(input: &'__r mut super::ABC<'d, C, E>) -> Self {
      SplitInto::split_into_mut(input)
    }
  }

  impl<
      'd,
      _R_a,
      _R_a0,
      _R_b,
      _R_c,
      _R_d,
      _R_x,
      _S_a,
      _S_a0,
      _S_b,
      _S_c,
      _S_d,
      _S_x,
      C,
      const E: u8,
    >
    SplitInto<
      super::ABC__Partial<'d, _R_a, _R_a0, _R_b, _R_c, _R_d, _R_x, C, E>,
      super::ABC__Partial<'d, _S_a, _S_a0, _S_b, _S_c, _S_d, _S_x, C, E>,
    > for super::ABC<'d, C, E>
  where
    Mut: CanSplitInto<_R_a, _S_a>,
    Mut: CanSplitInto<_R_a0, _S_a0>,
    Mut: CanSplitInto<_R_b, _S_b>,
    Mut: CanSplitInto<_R_c, _S_c>,
    Mut: CanSplitInto<_R_d, _S_d>,
    Mut: CanSplitInto<_R_x, _S_x>,
    C: Clone,
  {
    fn split_into<'__r>(
      input: &'__r super::ABC<'d, C, E>,
    ) -> (
      &'__r super::ABC__Partial<'d, _R_a, _R_a0, _R_b, _R_c, _R_d, _R_x, C, E>,
      &'__r super::ABC__Partial<'d, _S_a, _S_a0, _S_b, _S_c, _S_d, _S_x, C, E>,
    ) {
      unsafe {
        let input = input as *const _;
        (mem::transmute(input), mem::transmute(input))
      }
    }

    fn split_into_mut<'__r>(
      input: &'__r mut super::ABC<'d, C, E>,
    ) -> (
      &'__r mut super::ABC__Partial<
        'd,
        _R_a,
        _R_a0,
        _R_b,
        _R_c,
        _R_d,
        _R_x,
        C,
        E,
      >,
      &'__r mut super::ABC__Partial<
        'd,
        _S_a,
        _S_a0,
        _S_b,
        _S_c,
        _S_d,
        _S_x,
        C,
        E,
      >,
    ) {
      unsafe {
        let input = input as *mut _;
        (mem::transmute(input), mem::transmute(input))
      }
    }
  }

  impl<'d, _P_a, _P_a0, _P_b, _P_c, _P_d, _P_x, C, const E: u8> Deref
    for super::ABC__Partial<'d, _P_a, _P_a0, _P_b, _P_c, _P_d, _P_x, C, E>
  where
    _P_a: IsRef,
    _P_a0: IsRef,
    _P_b: IsRef,
    _P_c: IsRef,
    _P_d: IsRef,
    _P_x: IsRef,
    C: Clone,
  {
    type Target = super::ABC<'d, C, E>;

    fn deref(&self) -> &Self::Target {
      unsafe { mem::transmute(self) }
    }
  }
  pub struct Fields {
    pub a: usize,
    pub a0: usize,
    pub b: usize,
    pub c: usize,
    pub d: usize,
    pub x: usize,
  }
  pub const FIELDS: Fields = Fields {
    a: 0usize,
    a0: 1usize,
    b: 2usize,
    c: 3usize,
    d: 4usize,
    x: 5usize,
  };

  impl<'d, _P_a, _P_a0, _P_b, _P_c, _P_d, _P_x, _N, C, const E: u8>
    Adjust<_N, 0usize>
    for super::ABC__Partial<'d, _P_a, _P_a0, _P_b, _P_c, _P_d, _P_x, C, E>
  where
    C: Clone,
  {
    type Adjusted =
      super::ABC__Partial<'d, _N, _P_a0, _P_b, _P_c, _P_d, _P_x, C, E>;
  }

  impl<'d, _P_a, _P_a0, _P_b, _P_c, _P_d, _P_x, _N, C, const E: u8>
    Adjust<_N, 1usize>
    for super::ABC__Partial<'d, _P_a, _P_a0, _P_b, _P_c, _P_d, _P_x, C, E>
  where
    C: Clone,
  {
    type Adjusted =
      super::ABC__Partial<'d, _P_a, _N, _P_b, _P_c, _P_d, _P_x, C, E>;
  }

  impl<'d, _P_a, _P_a0, _P_b, _P_c, _P_d, _P_x, _N, C, const E: u8>
    Adjust<_N, 2usize>
    for super::ABC__Partial<'d, _P_a, _P_a0, _P_b, _P_c, _P_d, _P_x, C, E>
  where
    C: Clone,
  {
    type Adjusted =
      super::ABC__Partial<'d, _P_a, _P_a0, _N, _P_c, _P_d, _P_x, C, E>;
  }

  impl<'d, _P_a, _P_a0, _P_b, _P_c, _P_d, _P_x, _N, C, const E: u8>
    Adjust<_N, 3usize>
    for super::ABC__Partial<'d, _P_a, _P_a0, _P_b, _P_c, _P_d, _P_x, C, E>
  where
    C: Clone,
  {
    type Adjusted =
      super::ABC__Partial<'d, _P_a, _P_a0, _P_b, _N, _P_d, _P_x, C, E>;
  }

  impl<'d, _P_a, _P_a0, _P_b, _P_c, _P_d, _P_x, _N, C, const E: u8>
    Adjust<_N, 4usize>
    for super::ABC__Partial<'d, _P_a, _P_a0, _P_b, _P_c, _P_d, _P_x, C, E>
  where
    C: Clone,
  {
    type Adjusted =
      super::ABC__Partial<'d, _P_a, _P_a0, _P_b, _P_c, _N, _P_x, C, E>;
  }

  impl<'d, _P_a, _P_a0, _P_b, _P_c, _P_d, _P_x, _N, C, const E: u8>
    Adjust<_N, 5usize>
    for super::ABC__Partial<'d, _P_a, _P_a0, _P_b, _P_c, _P_d, _P_x, C, E>
  where
    C: Clone,
  {
    type Adjusted =
      super::ABC__Partial<'d, _P_a, _P_a0, _P_b, _P_c, _P_d, _N, C, E>;
  }
}
